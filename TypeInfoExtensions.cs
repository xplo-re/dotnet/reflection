﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Reflection;
using JetBrains.Annotations;


namespace XploRe.Reflection
{

    /// <summary>
    ///     Contains extension methods for <see cref="TypeInfo" /> instances.
    /// </summary>
    public static class TypeInfoExtensions
    {

        /// <summary>
        ///     Determines whether the specified type can be assigned to the current type.
        /// </summary>
        /// <remarks>
        ///     This is a polyfill implementation for .NET Standard.
        /// </remarks>
        /// <param name="typeInfo">This <see cref="TypeInfo" /> instance.</param>
        /// <param name="other">The type to check.</param>
        /// <returns><c>true</c> if the specified type can be assigned to this type, otherwise <c>false</c>.</returns>
        [Pure]
        public static bool IsAssignableFrom([NotNull] this TypeInfo typeInfo, [NotNull] Type other)
        {
            if (typeInfo == null) {
                throw new ArgumentNullException(nameof(typeInfo));
            }

            if (other == null) {
                throw new ArgumentNullException(nameof(other));
            }

            return typeInfo.IsAssignableFrom(other.GetTypeInfo());
        }

    }

}
